import csv

def load(filename):
    with open(filename, newline='') as csvfile:
        data = list(csv.reader(csvfile))
    return data[:][1:(len(data))], data[:][0]

def findCol(headers, colheader):
    index = 0
    for header in headers:
        if header == colheader:
            break
        index += 1
    return index

def GetCol(data, ratiocol):
    perrat = []
    for i in range(0, len(data)):
        perrat.append(data[i][ratiocol])
    return perrat

def GetColFromName(data, headers, name):
    colid = findCol(headers, name)
    return GetCol(data, colid)

def notNull(valuestr):
    if valuestr!= "#DIV/0!" and valuestr!= "0" and valuestr != "":
        return True
    else:
        return False

def GetDataFromName(headers, colheader, linedata):
    return linedata[findCol(headers, colheader)]

def GetDataFromNameWE(headers, colheader, linedata): #with error
    value = linedata[findCol(headers, colheader)]
    err1 = linedata[findCol(headers, colheader) + 1]
    err2 = linedata[findCol(headers, colheader) + 2]
    try:
        return [float(value), float(err1), float(err2)]
    except:
        return [0, 0, 0]

def BlankStr(leng):
    string = ","
    i = 0
    while(True):
        if len(string) != leng:
            string += ","
        elif len(string) == leng:
            return string
        elif i > 1000: #just in case we loop by some tragic mistake
            break
    return string

def arrToCSV(array):
    fullstr = ""
    for i in range(0, len(array)):
        if i != 0:
            fullstr += ","
        fullstr += array[i]
    return fullstr