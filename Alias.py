import CSV

class AliasList(object):
    def __init__(self, fn):
        #load the list
        data, headers = CSV.load(fn)
        self.koinames = CSV.GetColFromName(data, headers, "kepoi_name")
        self.altnames = CSV.GetColFromName(data, headers, "alt_name")
        self.kepnames = CSV.GetColFromName(data, headers, "kepler_name")
        self.n = len(self.koinames)
    
    def isKOIName(self, sysname):
        if sysname[0] == "K":
            if len(sysname) == 6:
                return True
            else:
               return False
        else:
            return False

    def getAltName(self, name):
        if self.isKOIName(name) == False:
            return name
        else:
            #get the full name
            if row != -1:
                row = self.getRowOfName(name)
                return self.altnames[row]
            else:
                return ""

    def getKoiName(self, name):
        if self.isKOIName(name):
            return name
        else:
            row = self.getRowOfName(name)
            if row != -1:
                row = self.getRowOfName(name)
                return self.koinames[row]
            else:
                return ""

    def getRowOfName(self, name):
        row = -1
        for i in range(0, self.n):
            if name == self.koinames[i] or name == self.altnames[i] or name == self.kepnames[i]:
                row = i
                break
        return row

    def addAliasList(self, aliaslist):
        self.koinames += aliaslist.koinames
        self.altnames += aliaslist.altnames
        self.kepnames += aliaslist.kepnames
        self.n = self.n + aliaslist.n
    
    def saveNewList(self, oldlen, koifn, outfn):
        with open(outfn, "w") as destination:
            k=0
            with open(koifn, "r") as source:
                #all koi stuff first
                for line in source:
                    destination.write(line)
                    k+=1
            #now for the additional
            for i in range(oldlen, self.n):
                #print(i, self.n)
                row = str(i) + ",,"
                koin = str(self.koinames[i]) + ","
                kepn = str(self.kepnames[i]) + ","
                blank = CSV.BlankStr(7)
                #print(blank)
                altn = str(self.altnames[i]) + blank
                newline = row + koin + kepn + altn
                destination.write(newline + "\n")
                k+=1 
        print("saved to:", outfn)