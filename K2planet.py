import planet
import CSV
import numpy as np

class K2CanPlanet(planet.CanPlanet):#inherit from candidate planet
    def __init__(self, rowi, linedata, headers):
        #row
        self.row = rowi
        #ID
        self.ID = CSV.GetDataFromName(headers, "epic_name", linedata) #ref/col id
        #koi name
        self.koiname = CSV.GetDataFromName(headers, "epic_candname", linedata) #kepoi_name
        self.plname = CSV.GetDataFromName(headers, "pl_name", linedata)
        if len(self.plname) > 0:
            strin = "EPIC 201488365"
            pllet = self.plname[-1]
            self.koiname = self.koiname[0:len(strin)]+" " + pllet
        self.sysname = super().getSysName()
        #confirmed, candidate or false (koi_disposition)
        self.canconf = CSV.GetDataFromName(headers, "k2c_disp", linedata)
        #mass data-get from confirmed list or use proxy
        self.mass = np.array([0,0,0])#zero for now
        #planet radius Earth
        self.rad = CSV.GetDataFromNameWE(headers, "pl_rade", linedata)#Earth Radii
        #last update
        self.lastupd =  ""
        #orbital period days
        self.orbper = CSV.GetDataFromNameWE(headers, "pl_orbper", linedata)
        #transit epoch
        self.transepoch = CSV.GetDataFromNameWE(headers, "pl_tranmid", linedata)
        #eccentricity
        self.eccn = [0, 0,0]
        #longitude of periastron
        self.longp = [0, 0,0]
        #transit duration
        self.ingress = [0, 0,0]
        #transit depth ppm
        self.depth = CSV.GetDataFromNameWE(headers, "pl_trandep", linedata)
        #stellar density (fitted)
        self.stardens = [0, 0,0]
        #orbital semi major axis
        self.semimajor = [0, 0,0]
        #inclination in degrees
        self.incl = CSV.GetDataFromNameWE(headers, "pl_orbincl", linedata)
        #planet star distance over star radius
        self.plstdistrad = CSV.GetDataFromNameWE(headers, "pl_ratdor", linedata)
        #number of planets in this system
        self.noplninsys = 0
        #star effective temperature
        self.starefftemp = CSV.GetDataFromNameWE(headers, "st_teff", linedata)
        #star radius
        self.srad = np.array(CSV.GetDataFromNameWE(headers, "st_rad", linedata))
        #star masses
        self.smass = np.array([0, 0,0])
        #star age
        self.sage = [0, 0,0]
        #kepmag
        self.kepmag = CSV.GetDataFromName(headers, "st_kep", linedata)
        self.gmag = 0
        self.rmag = 0
        self.imag = 0
        self.zmag = 0
        self.jmag = 0
        self.Hmag = 0
        self.Kmag = 0
        #planet star radius ratio
        #print(self.srad)
        self.plstradrat = planet.CalcRatio(self.rad, self.srad*planet.SrtoER)
        self.transN = 0
        #thestartomassratio
        #print(self.smass)
        self.stplmrat = planet.CalcRatio(self.mass, np.array(self.smass)*planet.SolarMassE)

class K2PlanetList(planet.CanPlanetList):
    def __init__(self, loadfn):
        self.planets = []
        self.uniquenames = []
        print("initalised planets")
        self.load(loadfn)

    def load(self, filename):
        data, headers = CSV.load(filename)
        self.planets = []
        for i in range(0, len(data)):
            loadedPlanet = K2CanPlanet(i, data[i], headers)
            #if this planet is candidate or confirmed, keep it
            if loadedPlanet.canconf != "FALSE POSITIVE":
                self.planets.append(loadedPlanet)
        print("loaded planets")
        self.removeDuplicates()
        self.n = len(self.planets)
        self.updateUniqueNames()
    
    def removeDuplicates(self):#keep best data though-well for per ratio anyway
        duplicateNames = []
        for i, pl in enumerate(self.planets):
            if self.doesNameClash(pl.koiname, i):
                duplicateNames.append(pl.koiname)
        #got a list of koinames
        for name in duplicateNames:#so for each see if we can find the confirmed
            pls, inds = self.getAllPlbyname(name)
            #now I have the planets, get the best
            confs, cinds = planet.anyConfirmed(pls)
            if len(confs) == 1:
                newpl = confs[0]
            else:
                if len(confs) > 1:
                    newpl = confs[planet.bestPer(confs)]#for now
                else:
                    newpl = pls[planet.bestPer(pls)]#for now
            #delete old and put in new
            for k in range(0, len(inds)):
                i = inds[k]-k
                del(self.planets[i])
            self.planets.append(newpl)
            
    def getAllPlbyname(self, name):
        pls = []
        inds = []
        for i, pl in enumerate(self.planets):
            if pl.koiname == name:
                pls.append(pl)
                inds.append(i)
        return pls, inds

    def doesNameClash(self, name, ind):
        clash = False
        for i, pl in enumerate(self.planets):
            if pl.koiname == name and ind != i:
                clash = True
        return clash
