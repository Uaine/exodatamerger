import Alias
import CSV

class K2AliasList(Alias.AliasList):#inherit but use same shape as base class
    def __init__(self, fn):
        data, headers = CSV.load(fn)
        self.koinames = CSV.GetColFromName(data, headers, "epic_host")#EPIC xxxxxxxx
        #still need the planet letter
        self.altnames = CSV.GetColFromName(data, headers, "alt_name")
        self.kepnames = CSV.GetColFromName(data, headers, "k2_name")#K2-xxx
        self.updatePlLets()
        self.n = len(self.koinames)
    
    def updatePlLets(self):
        for i, koi in enumerate(self.koinames):
            pllet = self.kepnames[i][-1]
            self.koinames[i] += " " + pllet

    def isKOIName(self, sysname):#override
        print(sysname[0:4])
        if sysname[0:4] == "EPIC":
            return True
        else:
            return False