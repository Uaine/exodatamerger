#generic/KOI planet lists to be inherited from for the other lists

import os
import CSV
import Alias
import math
import numpy as np

SolarMassE = 333030#Solar masses to Earth masses
SrtoER = 109.076#Slar Radii to Earth Radii

def anyConfirmed(plsarr):
    pls = []
    inds = []
    for i, pl in enumerate(plsarr):
        if pl.canconf == "CONFIRMED":
            pls.append(pl)
            inds.append(i)
    return pls, inds

def bestPer(plsarr):
    bestPer = -1
    k = -1
    for i, pl in enumerate(plsarr):
        if bestPer != -1:
            k = i
            bestPer = pl.orbper[1]
        elif pl.orbper[1] < bestPer:
            k = i
            bestPer = pl.orbper
    return k
def CalcRatio(outer, inner):
    try:
        value = outer[0]/inner[0]
        err1 = ((outer[1]/inner[0])**2. +((inner[1]*outer[0])**2./inner[0]**4.) )*(outer[0]/inner[0])
        err2 = -abs(((outer[2]/inner[0])**2. +((inner[2]*outer[0])**2./inner[0]**4.) )*(outer[0]/inner[0]))
    except:
        value = 0
        err1 = 0
        err2 = 0
    return [value, err1, err2]

class CanPlanet(object):#KOI
    def __init__(self, linedata, headers):
        #row
        self.row = int(CSV.GetDataFromName(headers, "rowid", linedata))
        #ID
        self.ID = int(CSV.GetDataFromName(headers, "kepid", linedata)) #ref/col id
        #koi name
        self.koiname = CSV.GetDataFromName(headers, "kepoi_name", linedata) #kepoi_name
        self.sysname = self.getSysName()
        #confirmed, candidate or false (koi_disposition)
        self.canconf = CSV.GetDataFromName(headers, "koi_disposition", linedata)
        #mass data-get from confirmed list or use proxy
        self.mass = np.array([0,0,0])#zero for now
        #planet radius Earth
        self.rad = CSV.GetDataFromNameWE(headers, "koi_prad", linedata)#Earth radii
        #last update
        self.lastupd =  CSV.GetDataFromName(headers, "koi_vet_date", linedata) #last update
        #orbital period days
        self.orbper = np.array(CSV.GetDataFromNameWE(headers, "koi_period", linedata))
        #transit epoch
        self.transepoch = CSV.GetDataFromNameWE(headers, "koi_time0", linedata)#the epoch-BJD
        #eccentricity
        self.eccn = CSV.GetDataFromNameWE(headers, "koi_eccen", linedata)
        #longitude of periastron
        self.longp = CSV.GetDataFromNameWE(headers, "koi_longp", linedata)#degrees
        #transit duration
        self.ingress = CSV.GetDataFromNameWE(headers, "koi_ingress", linedata)#transit duration in hours
        #transit depth ppm
        self.depth = CSV.GetDataFromNameWE(headers, "koi_depth", linedata)
        #planet star radius ratio
        self.plstradrat = CSV.GetDataFromNameWE(headers, "koi_ror", linedata)
        #stellar density (fitted)
        self.stardens = CSV.GetDataFromNameWE(headers, "koi_srho", linedata)#g/cm^3
        #orbital semi major axis
        self.semimajor = CSV.GetDataFromNameWE(headers, "koi_sma", linedata)#AU
        #inclination in degrees
        self.incl = CSV.GetDataFromNameWE(headers, "koi_incl", linedata)#deg
        #planet star distance over star radius
        self.plstdistrad = CSV.GetDataFromNameWE(headers, "koi_dor", linedata)
        #number of planets in this system
        self.noplninsys = CSV.GetDataFromName(headers, "koi_count", linedata)#int
        #star effective temperature
        self.starefftemp = CSV.GetDataFromNameWE(headers, "koi_steff", linedata)#Kelvin K
        #star radius
        self.srad = CSV.GetDataFromNameWE(headers, "koi_srad", linedata)#solar radii
        #star masses
        self.smass = CSV.GetDataFromNameWE(headers, "koi_smass", linedata)#solar masses
        #star age
        self.sage = CSV.GetDataFromNameWE(headers, "koi_sage", linedata)#Gyr
        #kepmag
        self.kepmag = CSV.GetDataFromName(headers, "koi_kepmag", linedata)
        self.gmag = CSV.GetDataFromName(headers, "koi_gmag", linedata)
        self.rmag = CSV.GetDataFromName(headers, "koi_rmag", linedata)
        self.imag = CSV.GetDataFromName(headers, "koi_imag", linedata)
        self.zmag = CSV.GetDataFromName(headers, "koi_zmag", linedata)
        self.jmag = CSV.GetDataFromName(headers, "koi_jmag", linedata)
        self.Hmag = CSV.GetDataFromName(headers, "koi_hmag", linedata)
        self.Kmag = CSV.GetDataFromName(headers, "koi_kmag", linedata)
        #transit number
        self.transN = CSV.GetDataFromName(headers, "koi_num_transits", linedata)
        if len(self.transN) == 0:
            self.transN = 0
        else:
            self.transN = int(self.transN)
        #thestartomassratio
        self.stplmrat = CalcRatio(self.mass, np.array(self.smass)*SolarMassE)

    def getSysName(self):
        return self.koiname[0:-3]

    def getLineToFile(self, newrow=0):#output to CSV line including line break
        row = str(newrow)
        id = str(self.ID)
        name = self.koiname
        canconf = self.canconf
        blank1 = CSV.BlankStr(9)#10 long
        orbper = str(self.orbper[0]) + CSV.BlankStr(1) + str(self.orbper[1]) + CSV.BlankStr(1) + str(self.orbper[2])
        blank2 = CSV.BlankStr(2)
        epoch = str(self.transepoch[0]) + CSV.BlankStr(1) + str(self.transepoch[1]) + CSV.BlankStr(1) + str(self.transepoch[2])
        ecc = str(self.eccn[0]) + CSV.BlankStr(1) + str(self.eccn[1]) + CSV.BlankStr(1) + str(self.eccn[2])
        longper = str(self.longp[0]) + CSV.BlankStr(1) + str(self.longp[1]) + CSV.BlankStr(1) + str(self.longp[2])
        blank3 = CSV.BlankStr(5)
        ingr = str(self.ingress[0]) + CSV.BlankStr(1) + str(self.ingress[1]) + CSV.BlankStr(1) + str(self.ingress[2])
        dp = str(self.depth[0]) + CSV.BlankStr(1) + str(self.depth[1]) + CSV.BlankStr(1) + str(self.depth[2])
        #st rad ratio
        stplr = str(self.plstradrat[0]) + CSV.BlankStr(1) + str(self.plstradrat[1]) + CSV.BlankStr(1) + str(self.plstradrat[2])
        #st dens
        stardens = str(self.stardens[0]) + CSV.BlankStr(1) + str(self.stardens[1]) + CSV.BlankStr(1) + str(self.stardens[2])
        #blank4 = CSV.BlankStr(1)
        rad = str(self.rad[0]) + CSV.BlankStr(1) + str(self.rad[1]) + CSV.BlankStr(1) + str(self.rad[2])
        sma = str(self.semimajor[0]) + CSV.BlankStr(1) + str(self.semimajor[1]) + CSV.BlankStr(1) + str(self.semimajor[2])
        incl = str(self.incl[0])+ CSV.BlankStr(1) + str(self.incl[1]) + CSV.BlankStr(1) + str(self.incl[2])
        blank5 = CSV.BlankStr(5)
        #planet star distance over star radius
        plstdisovrad = str(self.plstdistrad[0])+ CSV.BlankStr(1) + str(self.plstdistrad[1]) + CSV.BlankStr(1) + str(self.plstdistrad[2])
        blank6 = CSV.BlankStr(8)
        plno = str(self.noplninsys)
        noT = str(self.transN)
        blank7 = CSV.BlankStr(8)
        staret = str(self.starefftemp[0]) + CSV.BlankStr(1) + str(self.starefftemp[1]) + CSV.BlankStr(1) + str(self.starefftemp[2])
        blank8 = CSV.BlankStr(5)
        strad = str(self.srad[0]) + CSV.BlankStr(1) + str(self.srad[1]) + CSV.BlankStr(1) + str(self.srad[2])
        smass = str(self.mass[0]) + CSV.BlankStr(1) + str(self.mass[1]) + CSV.BlankStr(1) + str(self.mass[2])
        sage = str(self.sage[0])  + CSV.BlankStr(1) + str(self.sage[1]) + CSV.BlankStr(1) + str(self.sage[2])
        blank9 = CSV.BlankStr(2)
        kepmag = str(self.kepmag)
        blank10 = CSV.BlankStr(31)

        array = [row, id, name, "", canconf, blank1, 
        orbper, blank2, epoch, ecc, longper, blank3,
        ingr, dp, stplr, stardens, "", rad, sma,
        incl, blank5, plstdisovrad, blank6, plno,
        noT, blank7, staret, blank8, strad,
        smass, sage, blank9, kepmag, blank10]
        return CSV.arrToCSV(array)

class CanPlanetList(object):
    def __init__(self, loadfn):
        self.planets = []
        self.uniquenames = []
        print("initalised planets")
        self.load(loadfn)

    def load(self, filename):
        data, headers = CSV.load(filename)
        self.planets = []
        for i in range(0, len(data)):
            loadedPlanet = CanPlanet(data[i], headers)
            #if this planet is candidate or confirmed, keep it
            if loadedPlanet.canconf != "FALSE POSITIVE":
                self.planets.append(loadedPlanet)
        print("loaded planets")
        self.n = len(self.planets)
        self.updateUniqueNames()

    def addPlanet(self, pl):
        self.planets.append(pl)
        #print(len(self.planets))

    def UniqueSystemNames(self):
        uniquenames = []
        self.n = len(self.planets)
        for i in range(0, self.n):
            name = self.planets[i].sysname
            if name not in uniquenames:
                uniquenames.append(name)
        return uniquenames

    def updateUniqueNames(self):
        self.uniquenames = self.UniqueSystemNames()

    def getPlanetByRow(self, row):
        #row is 1 based index so ->
        ind = self.getIndexByRow(row)
        return self.planets[ind]
    
    def getIndexByRow(self, row):
        for i in range(0, len(self.planets)):
            if self.planets[i].row == row:
                return i    #return the index of the match

    def getPlanetByName(self, name):
        found = False
        for pl in self.planets:
            if pl.koiname == name:
                found = True
                return pl
        return found

    def saveNewList(self, oldlen, koifn, outfn):
        with open(outfn, "w") as destination:
            k=0
            with open(koifn, "r") as source:
                #all koi stuff first
                for line in source:
                    destination.write(line)
                    k+=1
            #now for the additional
            for i in range(oldlen, len(self.planets)):
                #print(k, len(self.planets))
                if self.planets[i].koiname != "":
                    newline = self.planets[i].getLineToFile(k)
                    destination.write(newline + "\n")
                    k+=1 
        print("saved to:", outfn)