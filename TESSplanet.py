import planet
import CSV
import numpy as np

class TCanPlanet(planet.CanPlanet):#inherit from candidate planet
    def __init__(self, rowi, linedata, headers):
        #row
        self.row = rowi
        #ID
        self.ID = int(CSV.GetDataFromName(headers, "tid", linedata)) #ref/col id
        #koi name
        self.koiname = self.getTOIName(CSV.GetDataFromName(headers, "toi", linedata)) #kepoi_name
        self.sysname = super().getSysName()
        #confirmed, candidate or false (koi_disposition)
        self.canconf = "CANDIDATE"
        #mass data-get from confirmed list or use proxy
        self.mass = np.array([0,0,0])#zero for now
        #planet radius Earth
        self.rad = self.getRad(linedata, headers)#Earth Radii
        #last update
        self.lastupd =  CSV.GetDataFromName(headers, "rowupdate", linedata)
        #orbital period days
        self.orbper = self.getPer(linedata, headers)
        #transit epoch
        self.transepoch = self.getTransEpoch(linedata, headers)
        #eccentricity
        self.eccn = [0, 0,0]
        #longitude of periastron
        self.longp = [0, 0,0]
        #transit duration
        self.ingress = [0, 0,0]
        #transit depth ppm
        self.depth = [0, 0,0]
        #stellar density (fitted)
        self.stardens = [0, 0,0]
        #orbital semi major axis
        self.semimajor = [0, 0,0]
        #inclination in degrees
        self.incl = [0, 0,0]
        #planet star distance over star radius
        self.plstdistrad = [0, 0,0]
        #number of planets in this system
        self.noplninsys = 0
        #star effective temperature
        self.starefftemp = self.getstarTemp(linedata, headers)
        #star radius
        self.srad = self.getStarRad(linedata, headers)
        #star masses
        self.smass = [0, 0,0]
        #star age
        self.sage = [0, 0,0]
        #kepmag
        self.kepmag = float(CSV.GetDataFromName(headers, "st_tmag", linedata))
        self.gmag = 0
        self.rmag = 0
        self.imag = 0
        self.zmag = 0
        self.jmag = 0
        self.Hmag = 0
        self.Kmag = 0
        #planet star radius ratio
        self.plstradrat = planet.CalcRatio(self.rad, self.srad*planet.SrtoER)
        self.transN = 0
        #thestartomassratio
        self.stplmrat = planet.CalcRatio(self.mass, np.array(self.smass)*planet.SolarMassE)

    def getTOIName(self, nam):
        n = len(nam)
        name = "T"
        for i in range(0, 8-n):
            name += "0"
        name += nam
        return name

    def getPer(self, linedata, headers):
        vstr = CSV.GetDataFromName(headers, "pl_orbper", linedata)
        if len(vstr) > 0:
            try:
                val = float(vstr)#Kelvin K
                err1 = float(CSV.GetDataFromName(headers, "pl_orbpererr1", linedata))
                err2 = float(CSV.GetDataFromName(headers, "pl_orbpererr2", linedata))
                return np.array([val, err1, err2])
            except:
                return np.array([0,0,0])
        else:
            return np.array([0,0,0])

    def getTransEpoch(self, linedata, headers):
        vstr = CSV.GetDataFromName(headers, "pl_tranmid", linedata)
        if len(vstr) > 0:
            val = float(vstr)#Kelvin K
            err1str = CSV.GetDataFromName(headers, "pl_tranmiderr1", linedata)
            err2str = CSV.GetDataFromName(headers, "pl_tranmiderr2", linedata)
            if len(err1str) > 0:
                err1 = float(err1str)
                err2 = float(err2str)
                return np.array([val, err1, err2])
            else:
                return np.array([val, 0, 0])
        else:
            return np.array([0,0,0])

    def getRad(self, linedata, headers):
        vstr = CSV.GetDataFromName(headers, "pl_rade", linedata)
        if len(vstr) > 0:
            val = float(vstr)#Kelvin K
            try:
                err1 = float(CSV.GetDataFromName(headers, "pl_radeerr1", linedata))
                err2 = float(CSV.GetDataFromName(headers, "pl_radeerr2", linedata))
                return np.array([val, err1, err2])
            except:
                return np.array([0,0,0])
        else:
            return np.array([0,0,0])

    def getstarTemp(self, linedata, headers):
        vstr = CSV.GetDataFromName(headers, "st_teff", linedata)
        if len(vstr) > 0:
            val = float(vstr)#Kelvin K
            errstr1 = CSV.GetDataFromName(headers, "st_tefferr1", linedata)
            errstr2 = CSV.GetDataFromName(headers, "st_tefferr2", linedata)
            if len(errstr1) > 0:
                err1 = str(errstr1)
                err2 = str(errstr2)
                return np.array([val, err1, err2])
            else:
                return np.array([val, 0, 0])
        else:
            return np.array([0,0,0])

    def getStarRad(self, linedata, headers):
        vstr = CSV.GetDataFromName(headers, "st_rad", linedata)
        if len(vstr) > 0:
            val = float(vstr)#Kelvin K
            try:
                err1 = float(CSV.GetDataFromName(headers, "st_raderr1", linedata))
                err2 = float(CSV.GetDataFromName(headers, "st_raderr2", linedata))
                return np.array([val, err1, err2])
            except:
                return np.array([0,0,0])
        else:
            return np.array([0,0,0])

class TPlanetList(planet.CanPlanetList):
    def __init__(self, loadfn):
        self.planets = []
        self.uniquenames = []
        print("initalised planets")
        self.load(loadfn)

    def load(self, filename):
        data, headers = CSV.load(filename)
        self.planets = []
        for i in range(0, len(data)):
            loadedPlanet = TCanPlanet(i, data[i], headers)
            #if this planet is candidate or confirmed, keep it
            if loadedPlanet.canconf != "FALSE POSITIVE":
                self.planets.append(loadedPlanet)
        print("loaded planets")
        self.n = len(self.planets)
        self.updateUniqueNames()