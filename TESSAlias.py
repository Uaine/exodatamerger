import Alias
import CSV

class TAliasList(Alias.AliasList):#inherit but use same shape as base class
    def __init__(self, fn):
        data, headers = CSV.load(fn)
        self.koinames = CSV.GetColFromName(data, headers, "TOIname")
        self.altnames = CSV.GetColFromName(data, headers, "Altname")
        self.kepnames = CSV.GetColFromName(data, headers, "TIC")#TIC names not kepnames
        self.n = len(self.koinames)
    
    def isKOIName(self, sysname):#override
        if sysname[0] == "T":
            if len(sysname) == 6:
                return True
            else:
               return False
        else:
            return False