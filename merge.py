#The main module to run
import planet
import TESSplanet
import K2planet
import Alias
import TESSAlias
import K2Alias
import os

def giveFN(name, checkExists=True):
    while(True):#loop until valid response
        outstr = "Please give filename for " + name + " list (including file extension):"
        print(outstr)
        res = input()
        if checkExists == False:
            break
        elif os.path.exists(res):
            break
        else:
            print("Path doesn't exist, please check/try again")
    return res

def mergeCandidates():
    koifn = giveFN("Kepler Candidates")
    tessfn = giveFN("TESS Candidates")
    K2fn = giveFN("K2 data")
    outfn = giveFN("output list", False)

    koi = planet.CanPlanetList(koifn)
    toi = TESSplanet.TPlanetList(tessfn)
    K2 = K2planet.K2PlanetList(K2fn)

    print("Loaded all planets in to the python engine, extending the KOI list with the additional ones...")
    oldlen = len(koi.planets)
    for i, pl in enumerate(toi.planets):
        koi.addPlanet(pl)
    for i, pl in enumerate(K2.planets):
        koi.addPlanet(pl)
    print("Added TOI planets complete")
    print("saving...")
    koi.saveNewList(oldlen, koifn, outfn)

def mergeAliases():
    koifn = giveFN("Kepler Alias")
    tessfn = giveFN("TESS Alias")
    K2fn = giveFN("K2 Alias")
    outfn = giveFN("output list", False)

    koi = Alias.AliasList(koifn)
    toi = TESSAlias.TAliasList(tessfn)
    K2 = K2Alias.K2AliasList(K2fn)

    print("loaded all alias lists, now extending the original with the additional ones...")
    oldlen = koi.n
    koi.addAliasList(toi)
    koi.addAliasList(K2)
    print("Combining complete, now out to file...")
    koi.saveNewList(oldlen, koifn, outfn)

ui = "0"
while(True):
    print("(0) merge candidates or (1) aliases into the one csv?")
    res = input()
    if res == "0" or res == "1":
        ui = res
        break
    else:
        print("please give either 0 or 1, try again")

if ui == "0":
    mergeCandidates()
elif ui == "1":
    mergeAliases()
